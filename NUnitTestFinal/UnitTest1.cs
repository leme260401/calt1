using FinalN00035221.Controllers;
using FinalN00035221.Models;
using NUnit.Framework;
using System.Collections.Generic;

namespace NUnitTestFinal
{
    public class Tests
    {
        [Test]
        public void uno()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 11, palo = 1 });
            cartas.Add(new Carta() { numero = 12, palo = 1 });
            cartas.Add(new Carta() { numero = 13, palo = 1 });
            cartas.Add(new Carta() { numero = 14, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.EscaleraColor(cartas);

            Assert.AreEqual("Escalera de color", resultado);
        }

        [Test]
        public void dos()
        {
            List<Carta> cartas = new List<Carta>();
            
            cartas.Add(new Carta() { numero = 5, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 1 });
            cartas.Add(new Carta() { numero = 7, palo = 1 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 9, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.EscaleraColor(cartas);

            Assert.AreEqual("Escalera de color", resultado);
        }

        [Test]
        public void tres()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 5, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 1 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 9, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.EscaleraColor(cartas);

            Assert.AreEqual("Otra", resultado);
        }

        [Test]
        public void cuatro()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 5, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 2 });
            cartas.Add(new Carta() { numero = 7, palo = 1 });
            cartas.Add(new Carta() { numero = 8, palo = 3 });
            cartas.Add(new Carta() { numero = 9, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.EscaleraColor(cartas);

            Assert.AreEqual("Otra", resultado);
        }

        [Test]
        public void cinco()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 2 });
            cartas.Add(new Carta() { numero = 10, palo = 3 });
            cartas.Add(new Carta() { numero = 10, palo = 4 });
 
            var controller = new HomeController();
            var resultado = controller.Poker(cartas);

            Assert.AreEqual("Poker", resultado);
        }

        [Test]
        public void seis()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 2 });
            cartas.Add(new Carta() { numero = 2, palo = 3 });
            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 4 });

            var controller = new HomeController();
            var resultado = controller.Poker(cartas);

            Assert.AreEqual("No Poker", resultado);
        }

        [Test]
        public void siete()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 1, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 1 });
            cartas.Add(new Carta() { numero = 4, palo = 1 });
            cartas.Add(new Carta() { numero = 5, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Color(cartas);

            Assert.AreEqual("Color", resultado);
        }
        [Test]
        public void ocho()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 2 });
            cartas.Add(new Carta() { numero = 4, palo = 3 });
            cartas.Add(new Carta() { numero = 5, palo = 4 });
            cartas.Add(new Carta() { numero = 6, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Color(cartas);

            Assert.AreEqual("No Color", resultado);
        }
        [Test]
        public void nueve()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 4, palo = 2 });
            cartas.Add(new Carta() { numero = 5, palo = 4 });
            cartas.Add(new Carta() { numero = 6, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Escalera(cartas);

            Assert.AreEqual("Escalera", resultado);
        }
        [Test]
        public void dies()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 7, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 4, palo = 2 });
            cartas.Add(new Carta() { numero = 4, palo = 4 });
            cartas.Add(new Carta() { numero = 5, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Escalera(cartas);

            Assert.AreEqual("No Escalera", resultado);
        }
        [Test]
        public void once()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 3 });
            cartas.Add(new Carta() { numero = 13, palo = 2 });
            cartas.Add(new Carta() { numero = 13, palo = 4 });
            cartas.Add(new Carta() { numero = 13, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Full(cartas);

            Assert.AreEqual("Full", resultado);
        }

        [Test]
        public void doce()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 9, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 3 });
            cartas.Add(new Carta() { numero = 13, palo = 2 });
            cartas.Add(new Carta() { numero = 13, palo = 4 });
            cartas.Add(new Carta() { numero = 13, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Full(cartas);

            Assert.AreEqual("No Full", resultado);
        }
        [Test]
        public void trece()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 13, palo = 2 });
            cartas.Add(new Carta() { numero = 13, palo = 4 });
            cartas.Add(new Carta() { numero = 13, palo = 1 });
            cartas.Add(new Carta() { numero = 4, palo = 1 });
            cartas.Add(new Carta() { numero = 7, palo = 3 });


            var controller = new HomeController();
            var resultado = controller.Trio(cartas);

            Assert.AreEqual("Trio", resultado);
        }
        [Test]
        public void catorce()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 4, palo = 1 });
            cartas.Add(new Carta() { numero = 4, palo = 3 });
            cartas.Add(new Carta() { numero = 6, palo = 2 });
            cartas.Add(new Carta() { numero = 13, palo = 4 });
            cartas.Add(new Carta() { numero = 13, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Trio(cartas);

            Assert.AreEqual("No Trio", resultado);
        }
        [Test]
        public void quince ()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 3 });
            cartas.Add(new Carta() { numero = 8, palo = 2 });
            cartas.Add(new Carta() { numero = 14, palo = 4 });
            cartas.Add(new Carta() { numero = 14, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.UnPar(cartas);

            Assert.AreEqual("Un Par", resultado);
        }
        [Test]
        public void dieciseis()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 3, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 3 });
            cartas.Add(new Carta() { numero = 6, palo = 2 });
            cartas.Add(new Carta() { numero = 8, palo = 4 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.UnPar(cartas);

            Assert.AreEqual("No Par", resultado);
        }
        [Test]
        public void diecisiete()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 6, palo = 3 });
            cartas.Add(new Carta() { numero = 6, palo = 2 });
            cartas.Add(new Carta() { numero = 8, palo = 4 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.DoblePar(cartas);

            Assert.AreEqual("Un Doble", resultado);
        }
        [Test]
        public void dieciocho()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 3 });
            cartas.Add(new Carta() { numero = 8, palo = 2 });
            cartas.Add(new Carta() { numero = 14, palo = 4 });
            cartas.Add(new Carta() { numero = 14, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.DoblePar(cartas);

            Assert.AreEqual("No Doble", resultado);
        }
            [Test]
            public void diecinueve ()
            {
                List<Carta> cartas = new List<Carta>();

                cartas.Add(new Carta() { numero = 2, palo = 1 });
                cartas.Add(new Carta() { numero = 6, palo = 3 });
                cartas.Add(new Carta() { numero = 8, palo = 2 });
                cartas.Add(new Carta() { numero = 14, palo = 4 });
                cartas.Add(new Carta() { numero = 14, palo = 1 });

                var controller = new HomeController();
                var resultado = controller.DoblePar(cartas);

                Assert.AreEqual("No Doble", resultado);
            }
        [Test]
        public void veinte()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 6, palo = 3 });
            cartas.Add(new Carta() { numero = 6, palo = 2 });
            cartas.Add(new Carta() { numero = 8, palo = 4 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.DoblePar(cartas);

            Assert.AreEqual("Un Doble", resultado);
        }
        [Test]
        public void veintiuno()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 11, palo = 1 });
            cartas.Add(new Carta() { numero = 12, palo = 1 });
            cartas.Add(new Carta() { numero = 13, palo = 1 });
            cartas.Add(new Carta() { numero = 14, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.EscaleraColor(cartas);

            Assert.AreEqual("Escalera de color", resultado);
        }

        [Test]
        public void veintidos()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 5, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 1 });
            cartas.Add(new Carta() { numero = 7, palo = 1 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 9, palo = 1 });

            var controller = new HomeController();

            var resultado = controller.EscaleraColor(cartas);

            Assert.AreEqual("Escalera de color", resultado);
        }

        [Test]
        public void veintitres()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 5, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 1 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 8, palo = 1 });
            cartas.Add(new Carta() { numero = 9, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.EscaleraColor(cartas);

            Assert.AreEqual("Otra", resultado);
        }

        [Test]
        public void veinticuatro()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 5, palo = 1 });
            cartas.Add(new Carta() { numero = 6, palo = 2 });
            cartas.Add(new Carta() { numero = 7, palo = 1 });
            cartas.Add(new Carta() { numero = 8, palo = 3 });
            cartas.Add(new Carta() { numero = 9, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.EscaleraColor(cartas);

            Assert.AreEqual("Otra", resultado);
        }

        [Test]
        public void veinticinco()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 2 });
            cartas.Add(new Carta() { numero = 10, palo = 3 });
            cartas.Add(new Carta() { numero = 10, palo = 4 });

            var controller = new HomeController();
            var resultado = controller.Poker(cartas);

            Assert.AreEqual("Poker", resultado);
        }

        [Test]
        public void veintiseis()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 2 });
            cartas.Add(new Carta() { numero = 2, palo = 3 });
            cartas.Add(new Carta() { numero = 10, palo = 1 });
            cartas.Add(new Carta() { numero = 10, palo = 4 });

            var controller = new HomeController();
            var resultado = controller.Poker(cartas);

            Assert.AreEqual("No Poker", resultado);
        }

        [Test]
        public void veintisiete()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 1, palo = 1 });
            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 1 });
            cartas.Add(new Carta() { numero = 4, palo = 1 });
            cartas.Add(new Carta() { numero = 5, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Color(cartas);

            Assert.AreEqual("Color", resultado);
        }
        [Test]
        public void veintiocho()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 2 });
            cartas.Add(new Carta() { numero = 4, palo = 3 });
            cartas.Add(new Carta() { numero = 5, palo = 4 });
            cartas.Add(new Carta() { numero = 6, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Color(cartas);

            Assert.AreEqual("No Color", resultado);
        }
        [Test]
        public void veintinueve()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 2, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 4, palo = 2 });
            cartas.Add(new Carta() { numero = 5, palo = 4 });
            cartas.Add(new Carta() { numero = 6, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Escalera(cartas);

            Assert.AreEqual("Escalera", resultado);
        }
        [Test]
        public void treinta()
        {
            List<Carta> cartas = new List<Carta>();

            cartas.Add(new Carta() { numero = 7, palo = 1 });
            cartas.Add(new Carta() { numero = 3, palo = 3 });
            cartas.Add(new Carta() { numero = 4, palo = 2 });
            cartas.Add(new Carta() { numero = 4, palo = 4 });
            cartas.Add(new Carta() { numero = 5, palo = 1 });

            var controller = new HomeController();
            var resultado = controller.Escalera(cartas);

            Assert.AreEqual("No Escalera", resultado);
        }
    }
    }
    
    

